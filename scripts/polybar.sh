#!/usr/bin/env sh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done

# Launch polybar
case "$(hostname)" in
    # "alucard")
    #     polybar top &
    #     ;;
    "dracula")
        polybar sc1 &
        polybar sc2 &
        ;;
    "alucard"|"trevor"|"LIN-PAR-TMAGALHAES-218300353P")
        if [ $(xrandr | grep connected | grep -v disconnected | wc -l) -eq 1 ]
        then
            polybar trevor
        else
            polybar trevor &
            polybar trevor2 &
        fi
        ;;
    *)
        return 0
        ;;
esac
