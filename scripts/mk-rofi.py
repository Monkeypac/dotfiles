import sys
import os

commands = dict()
commands["Keyboard: US"] = "bash ~/.scripts/mk-tools.sh X us"
commands["Keyboard: FR"] = "bash ~/.scripts/mk-tools.sh X fr"
commands["Screenshot"] = "pkill rofi && bash ~/.scripts/mk-tools.sh screenshot region"
commands[
    "Region Color"
] = "pkill rofi && notify-send $(bash ~/.scripts/mk-tools.sh screenshot region_color)"
commands["Volume: Up"] = "bash ~/.scripts/mk-tools.sh v u"
commands["Volume: Down"] = "bash ~/.scripts/mk-tools.sh v d"
commands["Volume: Toggle mute"] = "bash ~/.scripts/mk-tools.sh v t"
commands["Light: Up"] = "bash ~/.scripts/mk-tools.sh l u"
commands["Light: Down"] = "bash ~/.scripts/mk-tools.sh l d"
commands["Lock"] = "bash ~/.scripts/mk-tools.sh lock"
commands["Keyboard: Clean"] = "setxkbmap -option"
commands["Test notification"] = 'notify-send "Hello !" "rofi"'
commands[
    "Rofi: Http codes"
] = "pkill rofi && cat ~/.dotfiles/scripts/http-codes.html | rofi -markup-rows -dmenu"
commands[
    "Rofi: Ascii table"
] = "pkill rofi && cat ~/.dotfiles/scripts/ascii-table.html | rofi -sync -dmenu"
commands["Rofi: Run"] = "pkill rofi && rofi -show run"
commands["Rofi: Window"] = "pkill rofi && rofi -show window"
commands["Rofi: Keys"] = "pkill rofi && rofi -show keys"
commands["Rofi: GCP"] = "pkill rofi && bash ~/mkgcpr.sh"
commands["Quit"] = "pkill rofi"

arg = ""

if sys.argv[1:]:
    arg = " ".join(sys.argv[1:])

    if arg in commands:
        os.system(commands[arg])
        print(arg)

for key in commands:
    if key != arg:
        print(key)
