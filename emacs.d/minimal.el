(require 'package)

(setq package-enable-at-startup nil)

(add-to-list 'package-archives
	     '("org" . "http://orgmode.org/elpa/") t)
(add-to-list 'package-archives
	     '("gnu" . "https://elpa.gnu.org/packages/") t)
;; (add-to-list 'package-archives
;;              '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives
	     '("elpy" . "http://jorgenschaefer.github.io/packages/"))

(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile
  (require 'use-package))

(menu-bar-mode -1)
(setq scroll-step 1)
(setq scroll-conservatively 101)
(delete-selection-mode t)
(fset 'yes-or-no-p 'y-or-n-p)
(setq confirm-kill-emacs 'yes-or-no-p)
(show-paren-mode t)
(global-hl-line-mode +1)
(setq require-final-newline 't)
(global-auto-revert-mode 1)
(global-set-key (kbd "M-o") 'other-window)
(setq vc-handled-backends ())

(windmove-default-keybindings)

(defun mode-line-fill (reserve)
  "Return empty space using FACE and leaving RESERVE space on the right."
  (unless reserve
    (setq reserve 20))
  (when (and window-system (eq 'right (get-scroll-bar-mode)))
    (setq reserve (- reserve 3)))
  (propertize
   " "
   'display `((space :align-to
		     (-
		      (+
		       right right-fringe right-margin)
		                            ,reserve)))))

(setq-default
 mode-line-format
 (list
  ;; => Left side
  "%e" 'mode-line-front-space
  'mode-line-mule-info
  'mode-line-client
  'mode-line-modified
  'mode-line-remote
  " "
  'mode-line-buffer-identification
  " "
  'mode-name
  'flycheck-mode-line
  '(:eval (if (> (length vc-mode) 30)
	      (substring vc-mode 0 30)
	    vc-mode))

  ;; => Fill until the end of line but 15 characters
  (mode-line-fill 16)

  ;; => Right side
  ;; Line and column
  "%4l: %2c"
  " "
  ;; Percentage
  "%3p"
    'mode-line-end-spaces))

(use-package vlf
  :ensure t)

(use-package docker
  :ensure t
  :config (setq docker-container-ls-arguments '("--all" "--filter status=running")))

(use-package smex
  :ensure t
  :bind ("M-x" . smex))
(ido-mode 1)
(ido-everywhere 1)
(setq ido-default-file-method 'selected-window)
(setq ido-default-buffer-method 'selected-window)
(use-package ido-completing-read+
  :ensure t
  :config (ido-ubiquitous-mode 1))

(use-package which-key
  :defer 0.2
  :diminish
  :ensure t
    :config (which-key-mode))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(docker-container-shell-file-name "/bin/bash")
 '(shell-file-name "/bin/bash"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
