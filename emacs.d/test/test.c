
/* {{{ Test */
int toto() {
    int camel_case;

    return camel_case;a_veryeryeyrerklsjdflkjsdfiojwefjsoifjwlkjfosijflwekjflisjflk
}

void tata(void) {
    int i;

    return i;
}
/* {{{ Test 2 */

int try_harder(void)
{
    static int count = 0;

    return count++;
}

/* }}} */

/* {{{ Test 3 */

int try_harder2(void)
{
    static int count = 0;

    return count++;
}

/* {{{ Test 3.1 */

int try_harder3(void)
{
    static int count = 0;

    return count++;
}

/* }}} */

int try_harder4(void)
{
    static int count = 0;

    return count++;
}

/* }}} */

int try_softer(void)
{
    static int count = 255;

    return count--;
}


/* }}} */
