# -*- mode: conf; -*-

# {{{ Variables

set $mod Mod4
set $sc_1 DP-1
set $sc_2 eDP-1

# }}}
# {{{ Keybindings
# {{{ i3 related
# {{{ Movement

bindsym $mod+b focus left
bindsym $mod+n focus down
bindsym $mod+p focus up
bindsym $mod+f focus right

bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# Swith to last window
bindsym $mod+Tab [con_mark=_last] focus

# }}}
# {{{ Window management

bindsym $mod+Shift+B move left
bindsym $mod+Shift+N move down
bindsym $mod+Shift+P move up
bindsym $mod+Shift+F move right

bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# Kill focused window
bindsym $mod+Shift+A kill

# }}}
# {{{ Resize

# Resize window (you can also use the mouse for that)
mode "resize" {
    # These bindings trigger as soon as you enter the resize mode

    # Pressing left will shrink the window’s width.
    # Pressing right will grow the window’s width.
    # Pressing up will shrink the window’s height.
    # Pressing down will grow the window’s height.
    bindsym j resize shrink width 10 px or 10 ppt
    bindsym k resize grow height 10 px or 10 ppt
    bindsym l resize shrink height 10 px or 10 ppt
    bindsym m resize grow width 10 px or 10 ppt

    # same bindings, but for the arrow keys
    bindsym Left resize shrink width 10 px or 10 ppt
    bindsym Down resize grow height 10 px or 10 ppt
    bindsym Up resize shrink height 10 px or 10 ppt
    bindsym Right resize grow width 10 px or 10 ppt

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

# Press $mod+r to enter resize mode
bindsym $mod+r mode "resize"

# }}}
# {{{ Workspaces movement

set $ws_1 1:term
set $ws_2 2:code
set $ws_3 3:work
set $ws_4 4:
set $ws_5 5:
set $ws_6 6:web
set $ws_7 7:
set $ws_8 8:
set $ws_9 9:
set $ws_10 10:chat

# Set screens for workspaces
workspace $ws_1 output $sc_1
workspace $ws_2 output $sc_1
workspace $ws_3 output $sc_1
workspace $ws_4 output $sc_1
workspace $ws_5 output $sc_1
workspace $ws_6 output $sc_2
workspace $ws_7 output $sc_2
workspace $ws_8 output $sc_2
workspace $ws_9 output $sc_2
workspace $ws_10 output $sc_2

# Switch to workspace
bindsym $mod+1 workspace $ws_1
bindsym $mod+2 workspace $ws_2
bindsym $mod+3 workspace $ws_3
bindsym $mod+4 workspace $ws_4
bindsym $mod+5 workspace $ws_5
bindsym $mod+6 workspace $ws_6
bindsym $mod+7 workspace $ws_7
bindsym $mod+8 workspace $ws_8
bindsym $mod+9 workspace $ws_9
bindsym $mod+0 workspace $ws_10

# Move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws_1
bindsym $mod+Shift+2 move container to workspace $ws_2
bindsym $mod+Shift+3 move container to workspace $ws_3
bindsym $mod+Shift+4 move container to workspace $ws_4
bindsym $mod+Shift+5 move container to workspace $ws_5
bindsym $mod+Shift+6 move container to workspace $ws_6
bindsym $mod+Shift+7 move container to workspace $ws_7
bindsym $mod+Shift+8 move container to workspace $ws_8
bindsym $mod+Shift+9 move container to workspace $ws_9
bindsym $mod+Shift+0 move container to workspace $ws_10

# Next / Prev
bindsym $mod+comma workspace prev
bindsym $mod+period workspace next

# }}}
# {{{ Tiling
# {{{ Split mode

# Split horizontally
bindsym $mod+h split h

# Split Vertically
bindsym $mod+v split v

# }}}
# {{{ Layout

# Get tabs on top of each others
bindsym $mod+s layout stacking

# Get tabs side by side
bindsym $mod+z layout tabbed

# Split layout
bindsym $mod+e layout toggle split

bindsym $mod+g fullscreen

# }}}
# {{{ Floating

# Toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# Change focus between tiling / floating windows
bindsym $mod+x focus mode_toggle

# Use mouse + $mod to drag floating windows
floating_modifier $mod

# }}}
# {{{ Focus

# Focus the parent container
bindsym $mod+q focus parent

# Focus the child container
bindsym $mod+a focus child

bindsym $mod+i focus output left
bindsym $mod+o focus output right

# }}}
# {{{ Scratchpad

# Toggle scratchpad begin shown or not
bindsym $mod+space scratchpad show
bindsym $mod+Shift+o move scratchpad

# }}}
# }}}
# {{{ Others

# Reload the configuration file
bindsym $mod+Shift+c reload

# Restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# Exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# }}}
# }}}
# {{{ Custom

# Opening a term
bindsym $mod+Return exec bash ~/.scripts/mk-tools.sh shell

# Lock screen
bindsym $mod+l exec bash ~/.scripts/mk-tools.sh lock

# Screenshot
bindsym Print exec bash ~/.scripts/mk-tools.sh screenshot

# {{{ Media keys

# Increase sound volume
bindsym XF86AudioRaiseVolume exec --no-startup-id bash ~/.scripts/mk-tools.sh v u

# Decrease sound volume
bindsym XF86AudioLowerVolume exec --no-startup-id bash ~/.scripts/mk-tools.sh v d

# Toggle sound volume
bindsym XF86AudioMute exec --no-startup-id bash ~/.scripts/mk-tools.sh v t

# Increase screen brightness
bindsym XF86MonBrightnessUp exec bash ~/.scripts/mk-tools.sh l u

# Decrease screen brightness
bindsym XF86MonBrightnessDown exec bash ~/.scripts/mk-tools.sh l d

# }}}
# {{{ Rofi

# Start rofi run menu
bindsym $mod+d exec rofi -show run

# Sart rofi window select menu
bindsym $mod+k exec rofi -modi "MKMenu:~/.scripts/mk-rofi.sh" -show MKMenu
bindsym $mod+j exec rofi -modi "BotifyErrors:~/dev/help/botify_http_errors.sh" -show BotifyErrors
bindsym Mod1+Tab exec rofi -show window

# }}}

# }}}
# }}}
# {{{ Settings
# {{{ Font

font pango:monospace 8

# }}}
# {{{ i3 settings

# Hide top bar
for_window [class="^.*"] border pixel 1

# Hide side borders
hide_edge_borders both

# Print all window titles or not
for_window [class=".*"] title_format "%title"

focus_follows_mouse no

# }}}
# {{{ Bar

exec_always --no-startup-id bash ~/.scripts/polybar.sh &

# }}}
# {{{ X settings

exec --no-startup-id bash ~/.scripts/mk-tools.sh X reset
exec --no-startup-id compton

# }}}
# {{{ Notifications

exec dunst -config ~/.i3/dunst/dunstrc

# }}}
# {{{ Colors

# class                 border  backgr. text    indicator child_border
client.focused          #444444 #777777 #1b1a1c #e7d8b1
client.focused_inactive #444444 #444444 #888888 #AAAAAA
client.unfocused        #1b1a1c #1b1a1c #AAAAAA #AAAAAA
client.urgent           #CE4045 #CE4045 #e7d8b1 #DCCD69
client.background       #444444

# }}}
# }}}
