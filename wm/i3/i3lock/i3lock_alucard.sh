p=$(find ~/Misc/Pictures/locks -type f | sort -R | head -n 1)
delete = 0
# Only want png file
if echo "$p" | grep -q '.jpg$'; then
    delete=1
    no_ext=$(echo $p | sed 's/\.[^.]*$//')
    convert $no_ext.jpg $no_ext.png
    p=$no_ext.png
fi
echo $p >>/tmp/i3lock_debug.log
i3lock -t -e -i $p 2>>/tmp/i3lock_debug.log

if [ $delete ]; then
   rm $p
fi


