(defun find-pathname-recursive (pathname-list)
  (let ((possible-pathname (car pathname-list)))
    (if (probe-file possible-pathname)
        possible-pathname
      (find-pathname-recursive (cdr pathname-list)))))

(defun bat-pathname ()
  (let ((possible-pathnames
         '("/sys/class/power_supply/BAT1" "/sys/class/power_supply/BAT0")))
    (find-pathname-recursive possible-pathnames)))

(defun now-pathname ()
  (let ((possible-pathnames
         `(,(concatenate 'string (bat-pathname) "/" "energy_now")
           ,(concatenate 'string (bat-pathname) "/" "charge_now"))))
    (find-pathname-recursive possible-pathnames)))

(defun full-pathname ()
  (let ((possible-pathnames
         `(,(concatenate 'string (bat-pathname) "/" "energy_full")
           ,(concatenate 'string (bat-pathname) "/" "charge_full"))))
    (find-pathname-recursive possible-pathnames)))

(defun read-energy (name)
  (let ((energy-file (open name)))
    (read energy-file)))

(defcommand raise-backlight () ()
  "Raise backlight via light"
    (run-shell-command "light -A 10"))

(defcommand lower-backlight () ()
  "Lower backlight via light"
    (run-shell-command "light -U 10"))

(defcommand raise-volume () ()
  "Raise volume via amixer"
  (run-shell-command "pactl set-sink-volume 0 +5%"))

(defcommand lower-volume () ()
  "Lower volume"
  (run-shell-command "pactl set-sink-volume 0 -5%"))

(defcommand toggle-mute-volume () ()
  "Toggle volume mute via amixer"
  (run-shell-command "pactl set-sink-mute 0 toggle"))

(defcommand suspend () ()
  "Suspends the system (to RAM)"
  (progn
    (run-shell-command
     "xscreensaver-command -lock")
    (run-shell-command
     "sudo pm-suspend")))

(defcommand show-battery-state () ()
  "Shows the state of the first battery"
  (let ((energy-now (read-energy (now-pathname)))
        (energy-full (read-energy (full-pathname))))
    (echo (concatenate
           'string
           (write-to-string (float (* (/ energy-now energy-full) 100)))
           "%"))))

(defcommand screen-saver () ()
  "Turns off the screen until a key is pressed"
  (if (string= "dracula" (machine-instance))
      (run-shell-command "sh ~/.i3/i3lock/i3lock_dracula.sh")
      (if (string= "alucard" (machine-instance))
          (run-shell-command "sh ~/.i3/i3lock/i3lock_alucard.sh")
          (run-shell-command "sh ~/.i3/i3lock/i3lock.sh"))))

(defcommand firefox () ()
  "Start Firefox unless it is already running, in which case focus it."
  (run-or-raise "firefox" '(:class "Firefox")))

(defcommand chrome () ()
  "Start Chromium unless it is already running, in which case focus it."
  (run-or-raise "google-chrome-stable" '(:class "Google-chrome")))

(defcommand terminal () ()
  "Start urxvt unless it is already running, in which case focus it."
  (run-or-raise "urxvt" '(:class "urxvt")))

(defcommand terminal-raise () ()
  "Raise urxvt without starting it."
  (run-or-raise "" '(:class "URxvt")))
