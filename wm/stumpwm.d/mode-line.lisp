(setf *mode-line-timeout* 5)
(setf *mode-line-background-color* "#191C23")
(setf *mode-line-foreground-color* "#9BBBC6")
(setf *mode-line-border-color* "#191C23")
(setf *mode-line-position* :top)
(setf *mode-line-border-width* 0)
(setf *mode-line-pad-y* 0)

(load-module "cpu")
(if (string= "alucard" (machine-instance))
    (load-module "battery-portable"))
(load-module "mem")

(setf *screen-mode-line-format*
      (list
       ;; Workspace
       "^b[%n] "
       ;; Urgent frames
       "%u"
       ;; Goto right side
       "^>"
       ;; mail
       '(:eval (stumpwm:run-shell-command
                "cat ~/.unread_mail_count" t))
       " | "
       ;; battery
       (if (string= "alucard" (machine-instance))
           "%B | ")
       ;; cpu
       "%c %t | "
       ;; mem
       "%M| "
       ;; time
       '(:eval (stumpwm:run-shell-command "date +\"%H:%M:%S\"" t))))

;; autoload on first screen at least
(if (not (head-mode-line (current-head)))
    (toggle-mode-line (current-screen) (current-head)))
