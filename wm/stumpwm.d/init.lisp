(in-package :stumpwm)

(set-module-dir "~/.stumpwm.d/modules")
(load-module "windowtags")

(load "~/.stumpwm.d/utils.lisp")
(load "~/.stumpwm.d/key.lisp")
(load "~/.stumpwm.d/theme.lisp")
(load "~/.stumpwm.d/mode-line.lisp")

(defun x-setup-once ()
  (defvar *x-setup-initialized*)
  (if (boundp '*x-setup-initialized*)
      (message "Already initialized, so skipping X setup.")
      (progn
        (run-shell-command "xscreensaver &")
        (run-shell-command "dunst -config ~/.i3/dunst/dunstrc &")
        (setq *x-setup-initialized* t))))

(let ((home (sb-unix::posix-getenv "HOME")))

  ;; X setup
  (x-setup-once)

  ;; always start Emacs
  (emacs))

;; fr keyboard
(run-shell-command "setxkbmap fr")
(run-shell-command "setxkbmap -option caps:super")

;; key repeat rate
(run-shell-command "xset r rate 400 60")

;; Use Xresources
(run-shell-command "xrdb -merge ~/.Xresources")

;; host specific setup
;; Set mouse sensivity
(if (string= "dracula" (machine-instance))
    (progn
     ;; set mouse speed
     (run-shell-command "xset m 1/6")

     ;; correctly setup screens
     (run-shell-command
      "xrandr --output DVI-D-0 --primary --auto --output HDMI-0 --left-of DVI-D-0"))
  (run-shell-command "xrandr --output HDMI2 --primary --left-of HDMI1"))

(setq *window-format* "%n%s%80t [%20c]")
