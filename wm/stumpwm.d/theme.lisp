(set-font "-*-terminus-medium-*-*-*-12-*-*-*-*-*-*-*")

(set-msg-border-width 1)
(set-frame-outline-width 0)
(setf *maxsize-border-width* 1)
(setf *transient-border-width* 0)
(setf *normal-border-width* 1)
(setf *window-border-style* :thick)

(set-normal-gravity :center)
(setf *message-window-gravity* :bottom-left)
(setf *input-window-gravity* :bottom)

(set-fg-color "#9BBBC6")
(set-bg-color "#191C23")
(set-border-color "#9BBBC6")
(set-focus-color "#9BBBC6")
(set-unfocus-color   "#191C23")
(set-win-bg-color    "#191C23")
(setf *colors* (list "#323246"          ; 0 black
                     "#5A7882"          ; 1 red
                     "#8C8CA0"          ; 2 green
                     "#1E828C"          ; 3 yellow
                     "#3C788C"          ; 4 blue
                     "#6EA0B4"          ; 5 magenta
                     "#6E8CA0"          ; 6 cyan
                     "#96BEC8"          ; 7 white
                     "#506070"          ; 8 personal
                     "#705090"))        ; 9 personal
(update-color-map (current-screen))

(setf *mouse-focus-policy* :click)

;; Don't mess with the inc hints asked by X
(setf *ignore-wm-inc-hints* t)
