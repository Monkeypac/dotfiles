--
-- Notion tiling module configuration file
--

-- Bindings for the tilings.

defbindings("WTiling", {
    submap(META..META_KEY, {
        bdoc("Split current frame vertically."),
        kpress("2", "WTiling.split_at(_, _sub, 'bottom', false)"),

        bdoc("Split current frame horizontally."),
        kpress("3", "WTiling.split_at(_, _sub, 'right', false)"),
    }),

    bdoc("Go to frame above/below current frame."),
    kpress(META.."P", "ioncore.goto_next(_sub, 'up', {no_ascend=_})"),
    kpress(META.."Up", "ioncore.goto_next(_sub, 'up', {no_ascend=_})"),
    kpress(META.."N", "ioncore.goto_next(_sub, 'down', {no_ascend=_})"),
    kpress(META.."Down", "ioncore.goto_next(_sub, 'down', {no_ascend=_})"),
    mclick(META.."Shift+Button4", "ioncore.goto_next(_sub, 'up', {no_ascend=_})"),
    mclick(META.."Shift+Button5", "ioncore.goto_next(_sub, 'down', {no_ascend=_})"),

    bdoc("Go to frame right/left of current frame."),
    kpress(META.."Tab", "ioncore.goto_next(_sub, 'right')"),
    kpress(META.."Right", "ioncore.goto_next(_sub, 'right')"),
    kpress(META.."Left", "ioncore.goto_next(_sub, 'left')"),
    kpress(META.."F", "ioncore.goto_next(_sub, 'right')"),
    kpress(META.."B", "ioncore.goto_next(_sub, 'left')"),
    submap(META..META_KEY, {
        kpress("Tab", "ioncore.goto_next(_sub, 'left')"),

        kpress("O", "ioncore.goto_next(_sub, 'next', {no_ascend=_, no_descend=_})"),
        kpress("Shift+O", "ioncore.goto_next(_sub, 'prev', {no_ascend=_, no_descend=_})"),

        bdoc("Destroy current frame."),
        kpress("0", "WTiling.unsplit_at(_, _sub)"),
    }),
})


-- Frame bindings

defbindings("WFrame.floating", {
    submap(META..META_KEY, {
        bdoc("Tile frame, if no tiling exists on the workspace"),
        kpress("Q", "mod_tiling.mkbottom(_)"),
    }),
})

-- Context menu for tiled workspaces.

defctxmenu("WTiling", "Tiling", {
    menuentry("Destroy frame",
              "WTiling.unsplit_at(_, _sub)"),

    menuentry("Split vertically",
              "WTiling.split_at(_, _sub, 'bottom', true)"),
    menuentry("Split horizontally",
              "WTiling.split_at(_, _sub, 'right', true)"),

    menuentry("Flip", "WTiling.flip_at(_, _sub)"),
    menuentry("Transpose", "WTiling.transpose_at(_, _sub)"),

    menuentry("Untile", "mod_tiling.untile(_)"),

    submenu("Float split", {
        menuentry("At left",
                  "WTiling.set_floating_at(_, _sub, 'toggle', 'left')"),
        menuentry("At right",
                  "WTiling.set_floating_at(_, _sub, 'toggle', 'right')"),
        menuentry("Above",
                  "WTiling.set_floating_at(_, _sub, 'toggle', 'up')"),
        menuentry("Below",
                  "WTiling.set_floating_at(_, _sub, 'toggle', 'down')"),
    }),

    submenu("At root", {
        menuentry("Split vertically",
                  "WTiling.split_top(_, 'bottom')"),
        menuentry("Split horizontally",
                  "WTiling.split_top(_, 'right')"),
        menuentry("Flip", "WTiling.flip_at(_)"),
        menuentry("Transpose", "WTiling.transpose_at(_)"),
    }),
})


-- Extra context menu extra entries for floatframes.

defctxmenu("WFrame.floating", "Floating frame", {
    append=true,
    menuentry("New tiling", "mod_tiling.mkbottom(_)"),
})
