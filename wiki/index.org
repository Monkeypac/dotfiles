#+TITLE: Monkeypac's wiki
#+AUTHOR: Thomas "Monkeypac" Magalhaes

This wiki was designed for myself. Some stuff might not be apply-able
if not me.

* Linux
** Arch
*** Install
**** Preparation

***** Put archlinux.iso on USB stick

#+begin_src shell
dd bs=4M if=/path/to/archiso.iso of=/dev/sdx status=progress oflag=sync
#+end_src

***** Boot on USB stick
Boot options -> usb prioritary
Boot options -> uefi enabled

***** Internet connection
=wifi-menu= is installed.
Use it to connect to your local network and check connection.
#+begin_src shell
wifi-menu
ping www.google.com
#+end_src

***** Set time
ensure the system clock is accurate and check. You don't have a
timezone defined yet so don't worry, you will be UTC time.
#+begin_src shell
timedatectl set-ntp true
timedatectl status
#+end_src

**** Disks

***** Shred disk

Shred allows to put random values on a file / a disk.

=--random-source= is optional but writes random values from a source input.
=--iterations= says how many times you want to pass on the file.
#+begin_src shell
shred --verbose --random-source=/dev/urandom --iterations=3 /dev/sda
#+end_src

***** Partition
Label type: dos(if not uefi) / gpt

- 1 efi partion :: 250M //EFI
- 1 boot partition :: 250M //Linux filesystem
- 1 root partition :: >= 50G //Linux filesystem
- 1 home :: whatever left //Linux home
- 1 swap :: ~RAM //Linux swap
#+begin_src
cfdisk /dev/sda
#+end_src

Write all on disk.

***** Encryption
Do not encrypt =/boot= or =/efi= or =swap= partition. Not worth the effort.

Encrypt each necessary partition with:
Dont forget that if asked for majuscule "YES" it MUST be majuscule.
#+begin_src shell
cryptsetup --verbose --cipher aes-xts-plain64 --key-size 512 \
           --hash sha512 --iter-time 5000 --use-random luksFormat /dev/sdxY
#+end_src

Open each encrypted partition on name NAME:
#+begin_src shell
cryptsetup open --type luks /dev/sdxY NAME
#+end_src

This creates a device in =/dev/mapper= named NAME

***** Create the filesystems

Note that for the encrypted partitions, you have to mkfs the mapper
partition: =mkfs.ext4 /dev/mapper/NAME=.
#+begin_src shell
# efi
mkfs.vfat -F32 /dev/sda1

# normal partition, root, home, boot, ...
mkfs.ext4 /dev/sda1

# swap
mkswap /dev/sda4
#+end_src

***** Mount the filesystems

Again, if using encryption directly talk about the mapper one.

#+begin_src shell
mount -t ext4 /dev/mapper/cryptroot /mnt
mkdir -p /mnt/home
mkdir -p /mnt/boot
mount /dev/sda1 /mnt/boot
mkdir -p /mnt/boot
mkdir -p /mnt/boot/efi
mount /sda1 /mnt/boot/efi
mount -t ext4 /dev/mapper/crypthome /mnt/home
swapon /dev/sda1
#+end_src

***** Install the basics

#+begin_src shell
pacstrap /mnt base base-devel grub-efi-x86_64 zsh emacs git efibootmgr dialog wpa_supplicant openssh
#+end_src

***** Generate the fstab

#+begin_src shell
genfstab -pU /mnt >> /mnt/etc/fstab
#+end_src

**** Installation

***** Chroot on the new system

#+begin_src shell
arch-chroot /mnt
#+end_src

***** Timezone

#+begin_src shell
# set the timezone
ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime

# generate /etc/adjtime
hwclock --systohc --utc
#+end_src

***** Language

Whether do it in an editor or by hand but uncomment something in
/etc/locale.gen.

#+begin_src shell
# could also uncomment others like fr
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen

# generate the locale
locale-gen
#+end_src

Tell the system what language to use.

#+begin_src shell
echo LANG=en_US.UTF-8 > /etc/locale.conf
#+end_src

***** Host

Edit /etc/hostname:
#+begin_src shell
echo alucard > /etc/hostname
#+end_src

Add to /etc/hosts:
#+begin_src txt
127.0.0.1 localhost
::1       localhost
127.0.0.1 alucard.localdomain alucard
#+end_src

***** Users

#+begin_src shell
passwd
#+end_src

Creates a home for the user monkeypac.
#+begin_src shell
useradd -m -g users -G wheel -s /bin/zsh monkeypac
pacman -S sudo
EDITOR=emacs visudo
# go to '# %wheel ALL=(ALL) ALL'
# uncomment
#+end_src

***** Boot

Edit /etc/mkinitcpio.conf
#+begin_src text
HOOKS="... ... block encrypt filsystems ... ..."
#+end_src

Regen initramfs
#+begin_src shell
mkinitcpio -p linux
#+end_src

#+begin_src shell
grub-install
#+end_src

Edit /etc/default/grub
#+begin_src txt
GRUB_ENABLE_CRYPTODISK=y
GRUB_CMDLINE_LINUX="cryptdevice=/dev/disk/by-uuid/blublublbulbulbublu:cryptroot root=/dev/mapper/cryptroot resume=/dev/partswap quiet"
#+end_src

Edit /etc/crypt and add
#+begin_src txt
crypthome /dev/sda2
#+end_src

#+begin_src shell
grub-mkconfig --output /boot/grub/grub.cfg
#+end_src

**** The end

#+begin_src shell
exit
umount -R /mnt
swapoff -a
sytemctl reboot
#+end_src

***** Yaourt

[Deprecated]

Add to pacman.conf in=[options]:
#+begin_src text
ILoveCandy
#+end_src

#+begin_src shell
git clone https://aur.archlinux.org/package-query.git
cd package-query
makepkg -si
cd ..
git clone https://aur.archlinux.org/yaourt.git
cd yaourt
makepkg -si
cd ..
#+end_src

***** Yay

#+begin_src shell
pushd /tmp && sudo pacman -S --needed git base-devel && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si && popd
#+end_src

***** All packages

This should work. Didn't retest it.
There is a possibility for some packages to not exist but using yaourt it should be fine.
#+begin_src shell
while read p; do
    yaourt --noconfirm -S $p
done <~/.dotfiles/packages.list
#+end_src
*** yaourt / pacman
**** Help
For general options:
#+begin_src shell
yaourt --help
#+end_src
For options on an option:
#+begin_src shell
# Get help on the query (-Q) module
yaourt -Q --help
#+end_src
**** Update
This should be done quite often since Arch is on a rolling release model.
#+begin_src shell
yaourt -Syyu
#+end_src
*** Rasperry Pi
**** Auto wifi connect
Connect using wifi-menu.

#+BEGIN_SRC shell
netctl list
netctl enable wlan0-SSID
#+END_SRC
** Settings
*** alsamixer
Control sound using cli
** Git
*** Credentials
**** Avoid asking for password everytime
Beware that this makes the password kept as plain text.

The command to tell git to keep password is:
#+BEGIN_SRC shell
git config --global credential.helper store
#+END_SRC
This method can be improved by setting a timeout on the cache:
#+BEGIN_SRC shell
git config --global credential.helper 'cache --timeout 7200'
#+END_SRC
**** More info
#+BEGIN_SRC shell
git help credentials
#+END_SRC
** General
*** Users
Add new user.
-m stands for creating the home dir.
Possibility to say whether to use zsh or bash by default for this user.
#+BEGIN_SRC shell
useradd -m monkeypac
#+END_SRC
*** Date
Set current date.
#+BEGIN_SRC shell
timedatectl set-time "2017-10-15 14:20:00"
#+END_SRC
* Window Managers
** i3
- Tiling
- Many users
- conf reloadable
- not that much conf-able
- nice X support
- Good
** stumpwm
- Tiling
- Written in lisp
- Conf in lisp
- Conf-able as hell
- Conf reloadable
- Pretty good X support
- A little bit resources consuming
- Love it
** notionwm
- Tiling
- Conf in Lua
- Conf reloadable
- Conf-able as hell
- Good X support
- Really light in resources
** awesome
- Don't like
- Not tried for a long time
** bspwm                                                          :nottried:
* Emacs
Emacs is a big part of my workflow.
** batch mode
You can use emacs' commands without starting emacs.

Example:
#+begin_src shell
emacs --batch index.org -f org-html-export-to-html
#+end_src
is the same as while being in emacs:
#+begin_src shell
C-x C-f index.org RET
M-x org-html-export-to-html
#+end_src

This can be done with emacsclient too but you have to eval the file open:
#+begin_src shell
emacsclient -e '(progn (find-file "index.org") (org-html-export-to-html))'
#+end_src
** org-mode
*** HTML themes
**** Nice todo list
#+begin_src org
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.i3s.unice.fr/~malapert/css/worg.min.css"/>
#+HTML_HEAD: <script type="text/javascript" src="http://www.i3s.unice.fr/~malapert/js/ga.min.js"></script>
#+end_src
**** readtheorg
#+begin_src org
#+SETUPFILE: ~/.emacs.d/org-html-themes/setup/theme-readtheorg.setup
#+HTML_HEAD: <style> #table-of-contents::-webkit-scrollbar { display:none; } </style>
#+end_src
** Bindings
All bindings can be found using
#+begin_src shell
M-x describe-bindings
#+end_src
** Go

to install
#+begin_src shell
go get -u github.com/jstemmer/gotags
go get -u github.com/davidrjenni/reftools/cmd/fillstruct
go get -u github.com/x/...
go get -u github.com/rogpeppe/godef/...
go get -u github.com/mdempsky/gocode
go get -u golang.org/x/tools/cmd/goimports
go get -u golang.org/x/tools/cmd/guru
go get -u github.com/dougm/goflymake
go get -u github.com/fatih/gomodifytags
#+end_src

** Completion using LSP

*** to install

#+begin_src bash
# Python
pip install -U setuptools
sudo pip install jedi
sudo pip install python-language-server
sudo pip install 'python-language-server[rope]'
sudo pip install 'python-language-server[pyflakes]'
sudo pip install 'python-language-server[yapf]'

# Go
go get -u github.com/sourcegraph/go-langserver
#+end_src

* Go
** Using go with private dependencies

This allows to use ssh key for retrieving from github instead of https.
#+begin_src shell
git config --local url."git@github.com:".insteadOf "https://github.com/"
#+end_src

* Elastic search
Default port for ES is 9200.

View ES cluster:
#+begin_src bash :results output :exports both
curl http://localhost:9200
#+end_src

** List elastic api functions

#+begin_src bash :results output :exports both
curl http://localhost:9200/_cat
#+end_src

** Get cluster health

#+begin_src bash :results output :exports both
curl http://localhost:9200/_cat/health
#+end_src

#+begin_src bash :results output :exports both
curl http://localhost:9200/_cat/health?v
#+end_src

** List cluster indices

#+begin_src bash :results output :exports both
curl http://localhost:9200/_cat/indices
#+end_src

#+begin_src bash :results output :exports both
curl http://localhost:9200/_cat/indices?v
#+end_src

** View an index

#+begin_src bash :results output :exports both
curl http://localhost:9200/mon_index?pretty
#+end_src

** Search on an index

#+begin_src bash :results output :exports both
curl http://localhost:9200/mon_index/_search?pretty
#+end_src

#+begin_src bash :results output :exports both
curl http://localhost:9200/mon_index/_search?q=Marion | jq .
#+end_src

#+begin_src bash :results output :exports both
curl http://localhost:9200/mon_index/_search?q=employer:Marion | jq .
#+end_src

#+begin_src bash :results output :exports both
curl http://localhost:9200/mon_*/_search?q=Marion | jq .
#+end_src

** Get a specific document

#+begin_src bash :results output :exports both
curl http://localhost:9200/mon_index/_doc/1?pretty
#+end_src

** Create or update a document
#+begin_src bash :results output :exports both
curl -X PUT http://localhost:9200/mon_index/_doc/1?pretty -H 'Content-Type: application/json' -d'
{
    "employer": "LumApps",
    "city": 75000,
    "name": "Marion"
}
'
#+end_src

#+begin_src bash :results output :exports both
curl http://localhost:9200/mon_index/_doc/1 | jq .
#+end_src

* Misc
** GUI Browser
*** google-chrome (or google-chrome-stable)
*** firefox (or iceweasel)
*** qutebrowser
Nice browser based on vim bindings.

Allow usage of escape on websites:
#+begin_src conf
# ~/.config/qutebrowser/keys.conf
[!normal]

leave-mode
    <ctrl-[>
    <ctrl-g>

[normal]
# Keybindings for normal mode.

clear-keychain ;; search ;; fullscreen --leave
    <ctrl-g>
    <ctrl-[>

fake-key <escape>
    <escape>
#+end_src
This makes C-g the bind for escaping modes/commands/...
*** vivaldi
*** opera
** CLI Browser
*** elinks
One of the most known cli browsers
*** w3m
*** lynx
** Music
Music tools for command line
*** cmus
This is a cli tool for playing music
*** mpsyt
This is a cli tool for using youtube
** ImageViewer
*** feh
Often used for backgrounds
*** display
Kind of a default, quite often installed
*** qiv
A really nice imageviewer
** Reddit
*** rtv
Reddit Terminal Viewer

Works out of the box.
A really nice app. Allows to read reddit on cli. Can vote, can
comment, can post, good app.
A little problem with really long posts however.
Can login.
** CLI various
*** figlet
Print text as ascii art.
https://github.com/cmatsuoka/figlet

*** gti
For when you mispell git.
https://github.com/rwos/gti

*** bat
A "better" cat.
https://github.com/sharkdp/bat

*** direnv
Kinda like a bashrc when entering a directory.
Allows to have specific shell stuff for a specific directory.
Don't forget to add to zshrc:
#+begin_src shell
eval "$(direnv hook zsh)"
#+end_src
https://github.com/direnv/direnv

*** sl
For when you mispell =ls=.
https://github.com/mtoyoda/sl

*** moreutils
https://joeyh.name/code/moreutils/

*** GRV
Git Repository Viewer
https://github.com/rgburke/grv

*** Hexyl
HEX viewer
https://github.com/sharkdp/hexyl

*** Prettyping
ping wrapper
https://github.com/rgburke/grv

*** Log files viewer
https://github.com/tstack/lnav

*** Angle grinder
Another log tool but really particular.
https://github.com/rcoh/angle-grinder

*** JQ
JSON prettifyer
https://stedolan.github.io/jq/

*** Visidata
https://github.com/saulpw/visidata
